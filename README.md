# typesetting

[LaTex,  Pandoc,  MathJax]
descrition and configs of typesetting
組版の設定や説明など。


- [LaTex](https://www.latex-project.org/)
  - [LaTeX入門 - TeX Wiki](https://texwiki.texjp.org/?LaTeX%E5%85%A5%E9%96%80)
- [Pandoc - About pandoc](https://pandoc.org/)
  - [Pandoc - Pandoc User’s Guide](https://pandoc.org/MANUAL.html)
- [MathJax | Beautiful math in all browsers.](https://www.mathjax.org/)
