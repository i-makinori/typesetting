#!/bin/sh

# Directories/Files config
ORIGIN_FILES="composition.md"
HEAD_TEX_FILE="-H head.tex"
METADATA_FILE="--metadata-file=metadata.yaml"
OUTPUT_PDF_FILE="-o pand-pan-letter.pdf"

# Tex engine
TEX_ENGINE="--pdf-engine=lualatex"

# Contents meta
PAPER_SIZE="--variable=geometry:a5paper"
NUM_SECT="--number-sections"
TOC="--table-of-contents --toc-depth=2"
INDENT_PARA="-V indent=true"
CODES="-N --highlight-style=tango"
#CODES="--listings"
INCLUDER="--filter pandoc-include"

CONTENTS_META="$PAPER_SIZE $NUM_SECT $TOC $INDENT_PARA $CODES $INCLUDER"


<<'HTML'

HTML

#<<'TEX'
pandoc $HEAD_TEX_FILE $ORIGIN_FILES $METADATA_FILE $OUTPUT_PDF_FILE \
       $TEX_ENGINE $CONTENTS_META
#TEX
