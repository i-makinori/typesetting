
;; [両面印刷のプリンターで製本印刷をする - Ubuntu 18.04 LTS デスクトップガイド改](https://sicklylife.jp/ubuntu/1804/help/printing-booklet-duplex.html)

#|
ページ番号を次の順番で入力します (n は 4 の倍数となる合計ページ数に置き換えてください)。
n, 1, 2, n-1, n-2, 3, 4, n-3, n-4, 5, 6, n-5, n-6, 7, 8, n-7, n-8, 9, 10, n-9, n-10, 11, 12, n-11…
例:
4 ページの冊子の場合、4,1,2,3 と入力します。

8 ページの冊子の場合、8,1,2,7,6,3,4,5 と入力します。

20 ページの冊子の場合、20,1,2,19,18,3,4,17,16,5,6,15,14,7,8,13,12,9,10,11 と入力します。
|#



(defun page-aux (page)
  ;; reduce (++","++) Cons
  (format nil "~A,~A"
          page (updown 1 (- page 1))))

(defun updown (n m)
  (cond ((= 1 (abs (- m n))) (format nil "~A,~A" (min n m) (max n m)))
        ((= n m) n)
        ((> n m)
         (format nil "~A,~A,~A" n (- n 1)
                 (updown m (- n 2))))
        (t
         (format nil "~A,~A,~A" n (+ n 1)
                 (updown m (+ n 2))))))
