# 要約。組版を実行するコマンドについて。

1. `LaTeX`, `Pandoc` がインストールされていることを確認する。
1. `git clone` や `curl` や `wget` などで、[1] を落してくる。
1. メタデータたる、組版プログラムが文書の構造を定義する為に参照するデータの、ファイルの設定をする。
1. 原稿を変更してゆく。`pan-letter.sh`, `metadata.yaml` にて管理の為の情報の変更もする。
1. `$ ./pan-letter.sh` に依り、PDFファイルを生成する。
1. PDFファイルが生成されていることを確認し、`evince $FILE_NAME &` などで観覧する。

[1]: [Makinori Ikegami / typesetting · GitLab](https://gitlab.com/i-makinori/typesetting)
