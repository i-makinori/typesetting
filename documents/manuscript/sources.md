
<!-- 設定、編集。 -->



## pan-letter.sh


実行。及びメタデータ。[^not-yet]

[^not-yet]: 抽象化やコマンド引数に依るモードの切り替えなど、未完成な実装。


``` {.sh .numberLines}
!include ../pan-letter.sh
```

\newpage

## 自己言及的生成.sh

`tree~により一覧し、特定ディレクトリ以下は省略しつつも、自分自身のソースコードをインポートを指定するTexプログラム。

### 自己言及的な説明

```
tree
  - is
  - here

```

\newpage
## metadata.yaml

メタデータ。

``` {.yaml .numberLines}
!include ../metadata.yaml
```

\newpage
## head.tex

`metadata.yaml` では定義し切り難い文書構造の設定。

``` {.tex .numberLines}
!include ../head.tex
```


\newpage
## composition.md

文書の骨格。

``` {.markdown .numberLines}
!include ../composition.md
```

\newpage
## \{ Yet Another Manuscript Files \}.md

